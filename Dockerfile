FROM gcc
MAINTAINER Thejana-A
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "thejanaakmeemana@gmail.com"
RUN git config --global user.name "thejanaakmeemana"
